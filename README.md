Minimal docker environment to demonstrate a qemu bug on Apple M1 hardware when running with x86_64 platform.

## Environment

- MacOS 11.5.2 BigSUR
- M1 ARM64 Chip
- Docker Desktop Version 4.0.0

## Steps to emulate

`docker buildx build --iidfile build.id --platform linux/amd64 . --progress=plain`

`docker run --platform linux/amd64  `\`cat build.id\`` python -c "import tensorflow"`

## Output

`2021-09-06 13:35:24.435613: F tensorflow/core/lib/monitoring/sampler.cc:42] Check failed: bucket_limits_[i] > bucket_limits_[i - 1] (0 vs. 10)
qemu: uncaught target signal 6 (Aborted) - core dumped`
